import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../services/categorie.service';
import { Categories } from '../../shared/categories';
import { Http } from '@angular/http';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styles: [],
  providers: [CategoriesService]
})
export class CategoriesComponent implements OnInit {


  listCategories: Categories[];

  constructor(
      private CategorieServices: CategoriesService
  ) {

    this.CategorieServices.findAll().then(result => {
      result.subscribe(response => {
        this.listCategories = response.json();
        console.log(this.listCategories);
      });
    });

  }

  ngOnInit() {
  }

}
