import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class CategoriesService {


    url  = '../pages/categories/categories.json';

    constructor(private http: Http) {

    }

    /**
     * Recheche de tous les categories
     */
    findAll() {
        let json = this.http.get(this.url);
        return Promise.resolve(json);
    }

}