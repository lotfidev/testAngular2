import { Routes, RouterModule } from '@angular/router';
import { ProduitsComponent } from './pages/produits/produits.component';
import { CategoriesComponent } from './pages/categories/categories.component';
const MAINMENU_ROUTES: Routes = [
    { path: 'produits', component: ProduitsComponent },
    { path: 'categories', component: CategoriesComponent }
];
export const CONST_ROUTING = RouterModule.forRoot(MAINMENU_ROUTES);
